CREATE TABLE `products` (
  `id` int(3) PRIMARY KEY,
  `name` varchar(50),
  `cn_name` varchar(50),
  `brand_id` int(2),
  `desc` text,
  `cn_desc` text,
  `content` decimal(7,2),
  `content_unit_id` int(1),
  `specification` decimal(7,2),
  `specification_unit_id` int(1),
  `price` decimal(5,2),
  `dose` varchar(50),
  `cn_dose` varchar(50)
);

CREATE TABLE `unit` (
  `id` int(1) PRIMARY KEY,
  `name` varchar(10),
  `cn_name` varchar(10)
);

CREATE TABLE `expiry_date` (
  `id` int(4) PRIMARY KEY,
  `date` date
);

CREATE TABLE `item` (
  `id` int(6) PRIMARY KEY,
  `uniq_id` int(6),
  `shipping_id` int(5),
  `expiry_date_id` int(4),
  `product_id` int(3)
);

CREATE TABLE `shipping` (
  `id` int(4) PRIMARY KEY,
  `user_id` tinyint(2),
  `sent_date` date,
  `arrival_date` date,
  `status` tinyint(1),
  `weight` decimal(4,2),
  `cost` decimal(5,2),
  `original_shipping_id` varchar(15),
  `original_shipping_company_id` tinyint(1),
  `transferred_shippind_id` varchar(20),
  `transferred_shipping_company_id` tinyint(1)
);

CREATE TABLE `shipping_company` (
  `id` tinyint(1) PRIMARY KEY,
  `name` varchar(10)
);

CREATE TABLE `user` (
  `id` tinyint(2) PRIMARY KEY,
  `name` varchar(10)
);

CREATE TABLE `uniq` (
  `id` int(6) PRIMARY KEY,
  `uniq_id` char(10),
  `used` bool,
  `create_date` date
);

CREATE TABLE `brand` (
  `id` int(2) PRIMARY KEY,
  `name` varchar(50),
  `cn_name` varchar(50)
);

ALTER TABLE `item` ADD FOREIGN KEY (`uniq_id`) REFERENCES `uniq` (`id`);

ALTER TABLE `item` ADD FOREIGN KEY (`shipping_id`) REFERENCES `shipping` (`id`);

ALTER TABLE `item` ADD FOREIGN KEY (`expiry_date_id`) REFERENCES `expiry_date` (`id`);

ALTER TABLE `item` ADD FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

ALTER TABLE `shipping` ADD FOREIGN KEY (`original_shipping_company_id`) REFERENCES `shipping_company` (`id`);

ALTER TABLE `shipping` ADD FOREIGN KEY (`transferred_shipping_company_id`) REFERENCES `shipping_company` (`id`);

ALTER TABLE `shipping` ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

ALTER TABLE `products` ADD FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`);

ALTER TABLE `products` ADD FOREIGN KEY (`content_unit_id`) REFERENCES `unit` (`id`);

ALTER TABLE `products` ADD FOREIGN KEY (`specification_unit_id`) REFERENCES `unit` (`id`);
