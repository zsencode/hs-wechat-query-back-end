<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('user_id');
            $table->date('sent_date');
            $table->date('arrival_date');
            $table->tinyInteger('status');
            $table->decimal('weight',5,3);
            $table->decimal('cost',5,2);
            $table->string('original_shipping_id',15);
            $table->integer('original_shipping_company_id');
            $table->string('transferred_shipping_id',15);
            $table->integer('transferred_shipping_company_id');
            $table->foreign('original_shipping_company_id')->references('id')->on('shipping_company')->onDelete('cascade');
            $table->foreign('transferred_shipping_company_id')->references('id')->on('shipping_company')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping');
    }
}
