<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->integer('id',true);
            $table->string('name',50);
            $table->string('cn_name',50);
            $table->integer('brand_id');
            $table->text('desc');
            $table->text('cn_desc');
            $table->decimal('content',7,2);
            $table->integer('content_unit_id');
            $table->decimal('specification',7,2);
            $table->integer('specification_unit_id');
            $table->decimal('price',5,2);
            $table->string('dose',200);
            $table->string('cn_dose',200);
            $table->foreign('brand_id')->references('id')->on('brand')->onDelete('cascade');
            $table->foreign('content_unit_id')->references('id')->on('unit')->onDelete('cascade');
            $table->foreign('specification_unit_id')->references('id')->on('unit')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
