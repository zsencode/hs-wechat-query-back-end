<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('uniq_id');
            $table->integer('shipping_id');
            $table->integer('expiry_date_id');
            $table->integer('product_id');
            $table->foreign('uniq_id')->references('id')->on('uniq')->onDelete('cascade');
            $table->foreign('expiry_date_id')->references('id')->on('expiry_date')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
