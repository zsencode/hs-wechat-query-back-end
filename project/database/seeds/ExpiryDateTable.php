<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExpiryDateTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $format='Y-m-d';
        $now = date($format,time());
        $data=[['date'=> date($format,strtotime("+1years",strtotime($now)))],['date'=> date($format,strtotime("+1month",strtotime($now)))],['date'=> date($format,strtotime("+1weeks",strtotime($now)))]];
        DB::table('expiry_date')->insert($data);
    }
}
