<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UniqTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data=[['uniq_id'=>'AHTFG8YTS6','create_date'=>'2020-05-23'],['name'=>'98TGGSRFTI','create_date'=>'2020-05-23']];
        DB::table('uniq')->insert($data);
    }
}
