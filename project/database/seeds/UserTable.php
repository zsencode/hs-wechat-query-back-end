<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data=[['name'=>'冯淑娥'],['name'=>'田家文'],['name'=>'王春平'],['name'=>'谢翠华']];
        DB::table('user')->insert($data);
    }
}
