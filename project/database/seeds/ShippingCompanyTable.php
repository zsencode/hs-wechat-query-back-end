<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShippingCompanyTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=[['name'=>'美通快递'],['name'=>'韵达快递'],['name'=>'顺丰快递'],['name'=>'中国邮政']];
        DB::table('shipping_company')->insert($data);
    }
}
