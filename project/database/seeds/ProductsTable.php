<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data=[
            [
                'name'=>'Vitamin B6',
                'cn_name'=>'维生素B6',
                'brand_id'=>2,
                'desc'=>'Overview:\nVitamin B6, also called pyridoxine, is a water soluble vitamin that performs a wide variety of functions in your body. Life Brand B6 100 mg tablets provide you with a convenient way to supplement your diet with this important vitamin.\nBenefits:\nHelps the body to metabolize fats, proteins and carbohydrates.\nHelps in tissue formation.',
                'cn_desc'=>'概述：\n维生素B6，也称为吡哆醇，是一种水溶性维生素，可在您的体内发挥多种功能。 生命品牌B6 100毫克片剂为您提供一种方便的方法，以这种重要的维生素补充饮食。\n好处：\n帮助人体代谢脂肪，蛋白质和碳水化合物。\n帮助组织形成。',
                'content'=>100,
                'content_unit_id'=>2,
                'specification'=>100,
                'specification_unit_id'=>6,
                'price'=>8.99,
                'dose'=>'(Adults): Take 1 tablet daily or as directed by a health care practitioner. Consult a physician for use beyond 6 weeks.',
                'cn_dose'=>'（成人）：每天服用1片，或在保健医生的指导下服用。 咨询医生6周以上的使用时间。'
            ],
            [
                'name'=>'Cod Liver Oil',
                'cn_name'=>'鳕鱼肝油',
                'brand_id'=>2,
                'desc'=>'Overview:\nCod Liver Oil has been used for many years as a dietary supplement. It is an excellent source of vitamins A and D as well as omega-3 fatty acids. It has many benefits including the maintenance of eyesight and immune function, bones and teeth. This Life Brand formula provides 1250 IU of vitamins A and 400 IU of vitamin D in a convenient softgel.\nBenefits:\nHelps to maintain eyesight, skin, membranes and immune function.\nHelps in the development and maintenance of night vision.\nHelps in the development and maintenance of bones and teeth.\nHelps in the absorption and use of calcium and phosphorus.\nSource of vitamin D, a factor in the maintenance of good health.\nHelps to prevent vitamin D deficiency.\nA factor in the maintenance of good health.\nCalcium intake, when combined with sufficient Vitamin D, a healthy diet and regular exercise may reduce the risk of developing osteoporosis.\nHelps to prevent vitamin A deficiency.',
                'cn_desc'=>'概述：\ n鳕鱼肝油已被用作膳食补充剂多年。 它是维生素A和D以及omega-3脂肪酸的极好来源。 它具有许多好处，包括维持视力和免疫功能，骨骼和牙齿。 此生活品牌配方可在方便的软胶囊中提供1250 IU维生素A和400 IU维生素D。\ n优点：\ n有助于维持视力，皮肤，膜和免疫功能。\n有助于开发和维护夜视。\n帮助 \n有助于钙和磷的吸收和使用。\n维生素D的来源，是维持身体健康的一个因素。\n有助于预防维生素D缺乏。\n 保持健康。',
                'content'=>550,
                'content_unit_id'=>5,
                'specification'=>100,
                'specification_unit_id'=>7,
                'price'=>9.29,
                'dose'=>'Adults: Take 1 softgel 2 times daily or as directed by a health care practitioner.',
                'cn_dose'=>'成人：每天1次软胶囊2次，或在保健医生的指导下服用。'
            ],
        ];
        DB::table('products')->insert($data);
    }
}
