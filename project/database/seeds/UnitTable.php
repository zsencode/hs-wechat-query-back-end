<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data=[
            ['name'=>'','cn_name'=>''],
            ['name'=>'mg','cn_name'=>'毫克'],
            ['name'=>'g','cn_name'=>'克'],
            ['name'=>'ml','cn_name'=>'毫升'],
            ['name'=>'iu','cn_name'=>'国际单位'],
            ['name'=>'tablets','cn_name'=>'片'],
            ['name'=>'softgels','cn_name'=>'胶囊'],
        ];
        DB::table('unit')->insert($data);
    }
}
