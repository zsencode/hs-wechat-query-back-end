<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data=[
            ['uniq_id'=>'1','shipping_id'=>'1','expiry_date_id'=>'1','product_id'=>'1'],
            ['uniq_id'=>'2','shipping_id'=>'2','expiry_date_id'=>'2','product_id'=>'2'],
        ];
        DB::table('item')->insert($data);
    }
}
