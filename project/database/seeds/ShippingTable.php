<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShippingTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data=[
            [
                'user_id'=>1,
                'sent_date'=>'2020-04-30',
                'arrival_date'=>'2020-05-24',
                'status'=>1,
                'weight'=>3.190,
                'cost'=>35.50,
                'original_shipping_id'=>'VE010296378CA',
                'original_shipping_company_id'=>1,
                'transferred_shipping_id'=>'7700153676917',
                'transferred_shipping_company_id'=>2,
            ],
            [
                'user_id'=>1,
                'sent_date'=>'2020-03-30',
                'arrival_date'=>'2020-04-24',
                'status'=>1,
                'weight'=>2.170,
                'cost'=>25.50,
                'original_shipping_id'=>'VE010297051CA',
                'original_shipping_company_id'=>1,
                'transferred_shipping_id'=>'7700154782022',
                'transferred_shipping_company_id'=>2,
            ],
        ];
        DB::table('shipping')->insert($data);
    }
}
