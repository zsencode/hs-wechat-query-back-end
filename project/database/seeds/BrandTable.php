<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data=[['name'=>'Oltimo','cn_name'=>'康盈素'],['name'=>'Life','cn_name'=>'生命'],];
        DB::table('brand')->insert($data);
    }
}
