<?php

namespace App\Http\Controllers;
use App\Query;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function testQuery($id){
        return $id;
    }
    public function queryItem(Request $request)
    {

        $arr['id']=trim($request->route('id'));
        $validator = Validator::make($arr, [
            'id' => 'required|size:10',
        ]);
        if ($validator->fails()) {
            return response()->json(['errorcode'=>'1','data'=>'','message'=>'bad request format'],200,['Content-type: application/json; charset=utf-8']);
        }
        $res=json_decode(Query::item($arr['id']));
        if(empty($res))
        {
            return response()->json(['errorcode'=>'2','data'=>'','message'=>'no result found'],200,['Content-type: application/json; charset=utf-8']);
        }
        return response()->json(['errorcode'=>'0','data'=>Query::item($arr['id']),'message'=>'ok'],200,['Content-type: application/json; charset=utf-8']);

    }
}
