<?php

namespace App;
use Illuminate\Support\Facades\DB;


class Query
{
    public static function item($id)
    {
       return DB::table('item')
        ->join('uniq', function ($join) {
            $join->on('item.uniq_id', '=', 'uniq.id');
        })
        ->join('shipping', function ($join) {
            $join->on('item.shipping_id', '=', 'shipping.id');
        })
        ->join('shipping_company as original_shipping_company', function ($join) {
            $join->on('shipping.original_shipping_company_id', '=', 'original_shipping_company.id');
        })
        ->join('shipping_company as transferred_shipping_company', function ($join) {
            $join->on('shipping.transferred_shipping_company_id', '=', 'transferred_shipping_company.id');
        })
        ->join('user', function ($join) {
            $join->on('shipping.user_id', '=', 'user.id');
        })
        ->join('expiry_date', function ($join) {
            $join->on('item.expiry_date_id', '=', 'expiry_date.id');
        })
        ->join('products', function ($join) {
            $join->on('item.product_id', '=', 'products.id');
        })
        ->join('brand', function ($join) {
            $join->on('products.brand_id', '=', 'brand.id');
        })
        ->join('unit as content_unit', function ($join) {
            $join->on('products.content_unit_id', '=', 'content_unit.id');
        })
        ->join('unit as specification_unit', function ($join) {
            $join->on('products.specification_unit_id', '=', 'specification_unit.id');
        })
        ->where('uniq.uniq_id','=',$id)
        ->get(
            [
                'brand.cn_name as brand_cn_name',
                'expiry_date.date as expiry_date',
                'content_unit.cn_name as content_unit_name',
                'specification_unit.cn_name as specification_unit_name',
                'original_shipping_company.name as original_shipping_company_name',
                'transferred_shipping_company.name as transferred_shipping_company_name',
                'products.cn_name as product_name',
                'products.cn_desc as product_desc',
                'products.content as product_content',
                'products.specification as product_specification',
                'products.cn_dose as product_dose',
                'shipping.sent_date as shipping_sent_date',
                'shipping.arrival_date as shipping_arrival_date',
                'shipping.status as shipping_status',
                'shipping.original_shipping_id as shipping_original_id',
                'shipping.transferred_shipping_id as shipping_transferred_id',
                'original_shipping_company.name as original_shipping_company_name',
                'transferred_shipping_company.name as transferred_shipping_company_name',
                ]
            );
    }
}
