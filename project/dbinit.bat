php artisan migrate
php artisan db:seed --class=UnitTable
php artisan db:seed --class=UserTable
php artisan db:seed --class=ShippingCompanyTable
php artisan db:seed --class=UniqTable
php artisan db:seed --class=ExpiryDateTable
php artisan db:seed --class=BrandTable
php artisan db:seed --class=ProductsTable
php artisan db:seed --class=ItemTable
php artisan db:seed --class=ShippingTable
pause;


