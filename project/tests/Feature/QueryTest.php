<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Config;

class QueryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_Item()
    {
        $this->withoutExceptionHandling();
        $response = $this->get('http://127.0.0.1:8000/api/v1/queryitem/AHTFG8YTS');
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'errorcode'=>'1',
        ]);
        $response = $this->get('http://127.0.0.1:8000/api/v1/queryitem/AHTFG8YTS5');
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'errorcode'=>'2',
        ]);
        $response = $this->get('http://127.0.0.1:8000/api/v1/queryitem/AHTFG8YTS6');
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'errorcode'=>'0',
        ]);

    }
}
