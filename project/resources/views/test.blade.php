<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Request Sample</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 54px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                width:33%;

                margin-bottom: 30px;
                border-style: solid;
                border-width:2px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class=" m-b-md">
                    <div class="title">
                         错误请求类型
                    </div>
                    <div>
                        <p id="wrongFormat"></p>
                    </div>
                </div>
                <div class=" m-b-md">
                    <div class="title">
                        不存在请求值
                    </div>
                    <div>
                        <p id="notExist"></p>
                    </div>
                </div>
                <div class=" m-b-md">
                    <div class="title">
                        正确请求
                    </div>
                    <div>
                        <p id="correctResponse"></p>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script type="text/javascript"  charset="utf-8">
        axios.get('http://localhost:8000/api/v1/queryitem/AHTFG8YTS6')
        .then(function (response) {

            console.log(response);
            document.getElementById("correctResponse").innerHTML=`产品名称：${response.data.data[0].brand_cn_name}${response.data.data[0].product_name} <br>
            过期日期：${response.data.data[0].expiry_date} <br>
            介绍：${response.data.data[0].product_desc.split('\\n').join('<br>')} <br>
            推荐用量：${response.data.data[0].product_dose} <br>
            产品含量：${response.data.data[0].product_content} ${response.data.data[0].content_unit_name} <br>
            包装规格：${response.data.data[0].product_specification} ${response.data.data[0].specification_unit_name} <br>
            始发快递：${response.data.data[0].original_shipping_company_name} <br>
            始发单号：${response.data.data[0].shipping_original_id} <br>
            转运快递：${response.data.data[0].transferred_shipping_company_name} <br>
            转运快递：${response.data.data[0].shipping_transferred_id} <br>
            是否送达：${response.data.data[0].shipping_status==0?'否':'是'} <br>
            发件日期：${response.data.data[0].shipping_sent_date} <br>
            送达日期：${response.data.data[0].shipping_arrival_date} <br>
            `;
        })
        axios.get('http://localhost:8000/api/v1/queryitem/AHTFG8YTS5')
        .then(function (response) {
            document.getElementById("notExist").innerHTML='请求码不存在';
        })

        axios.get('http://localhost:8000/api/v1/queryitem/AHTFG8YTS')
        .then(function (response) {
            document.getElementById("wrongFormat").innerHTML='请求数据格式错误';
        })

    </script>
</html>
